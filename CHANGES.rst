prgconfig 2.0.0-alpha.0 (2022-10-17)
====================================

Features
--------

- Use `tomlkit` instead of `toml`. (#1)
- Add support to Windows and MacOS config file location. (#2)
- Add getcheck() method that fetch a element of the configuration and
  check its type. If type does not match it raises TypeError. (#3)
- Write the current configuration. (#5)


Deprecations and Removals
-------------------------

- Remove support of python < 3.6. (#1)


prgconfig 1.0.0 (2022-10-15)
============================

Deprecations and Removals
-------------------------

- Version 2.0.0 will drop support of python older than 3.6 (at least).
  (python-support)


prgconfig 1.0.0-beta.3 (2021-10-29)
===================================

Features
--------

- PrgConfig accept filename as string or Path object. Both can be used. (#4)


prgconfig 1.0.0-beta.2 (2020-12-12)
===================================

Features
--------

- Change name of default_file_path property (names)
- Change to poetry build system (poetry)


Improved Documentation
----------------------

- Enhance readme file. (readme)


prgconfig 1.0.0-beta.1 (2020-01-05)
===================================

Feature
-------

* Rename project.
* Add test with other python version.


prgconfig 1.0.0-beta (2019-11-24)
=================================

Feature
-------

* Fully tested.
* Use proper default file location for configuration file.
* Installation tested
* Need to be used by other applications before going to version 1.0.0.
