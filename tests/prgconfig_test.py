"""Test for prgconfig package"""

import filecmp
import os
import tempfile
from pathlib import Path

import pytest

import prgconfig as config


class TestPrgConfig:
    """Test suite for PrgConfig"""

    @staticmethod
    def test_xdg_constant():
        """Test XDG var"""
        os.environ["XDG_CONFIG_HOME"] = "/home/test/.config"
        os.environ["XDG_CONFIG_DIRS"] = "/etc/xdg1:/etc/xdg2:/etc/xdg3"
        conf = config.PrgConfig(prg_name="test")

        assert conf.xdg_config_home == Path("/home/test/.config")
        assert conf.xdg_config_dirs == [
            Path("/etc/xdg1"),
            Path("/etc/xdg2"),
            Path("/etc/xdg3"),
        ]

    @staticmethod
    def test_getplatform():
        """Test getplatform method"""
        assert config.PrgConfig.getplatform() != "Windows"
        assert config.PrgConfig.getplatform() != "Darwin"

    @staticmethod
    def test_init():
        """Test init of a PrgConfig."""
        os.environ["XDG_CONFIG_HOME"] = "/home/test/.config"
        os.environ["XDG_CONFIG_DIRS"] = "/etc/xdg1:/etc/xdg2"

        prgname = "test-prg"
        conf = config.PrgConfig(prgname)
        assert conf.prg_name == prgname
        paths = []

        # Current directory
        paths.append(Path(f"{prgname}.conf"))
        paths.append(Path(f"{prgname}.toml"))
        paths.append(Path(f".{prgname}.conf"))
        paths.append(Path(f".{prgname}.toml"))

        # In xdg_config_home
        paths.append(Path(f"/home/test/.config/{prgname}.conf"))
        paths.append(Path(f"/home/test/.config/{prgname}.toml"))
        paths.append(Path(f"/home/test/.config/{prgname}/config"))
        paths.append(Path(f"/home/test/.config/{prgname}/{prgname}.conf"))
        paths.append(Path(f"/home/test/.config/{prgname}/{prgname}.toml"))

        # In home
        paths.append(Path().home() / f".{prgname}.conf")
        paths.append(Path().home() / f".{prgname}.toml")

        # In xdg_config_dirs
        paths.append(Path(f"/etc/xdg1/{prgname}.conf"))
        paths.append(Path(f"/etc/xdg1/{prgname}.toml"))
        paths.append(Path(f"/etc/xdg1/{prgname}/config"))
        paths.append(Path(f"/etc/xdg1/{prgname}/{prgname}.conf"))
        paths.append(Path(f"/etc/xdg1/{prgname}/{prgname}.toml"))
        paths.append(Path(f"/etc/xdg2/{prgname}.conf"))
        paths.append(Path(f"/etc/xdg2/{prgname}.toml"))
        paths.append(Path(f"/etc/xdg2/{prgname}/config"))
        paths.append(Path(f"/etc/xdg2/{prgname}/{prgname}.conf"))
        paths.append(Path(f"/etc/xdg2/{prgname}/{prgname}.toml"))

        # /etc
        paths.append(Path(f"/etc/{prgname}.conf"))
        paths.append(Path(f"/etc/{prgname}.toml"))
        paths.append(Path(f"/etc/{prgname}/config"))
        paths.append(Path(f"/etc/{prgname}/{prgname}.conf"))
        paths.append(Path(f"/etc/{prgname}/{prgname}.toml"))

        for path in paths:
            assert path in conf.config_file_path
        assert paths == conf.config_file_path

    @staticmethod
    def test_init_windows():
        """Test init of a PrgConfig on Windows."""

        prgname = "test-prg"
        conf = config.PrgConfig(prgname)
        # Spoof the platform function
        conf.getplatform = lambda: "Windows"
        assert conf.getplatform() == "Windows"
        conf.config_file_path = conf.default_config_file_path
        paths = []

        # Current directory
        paths.append(Path(f"{prgname}.conf"))
        paths.append(Path(f"{prgname}.toml"))
        paths.append(Path(f".{prgname}.conf"))
        paths.append(Path(f".{prgname}.toml"))

        # In windows dir
        paths.append(Path().home() / f"AppData/Roaming/{prgname}.conf")
        paths.append(Path().home() / f"AppData/Roaming/{prgname}.toml")
        paths.append(
            Path().home() / f"AppData/Roaming/{prgname}/{prgname}.conf"
        )
        paths.append(
            Path().home() / f"AppData/Roaming/{prgname}/{prgname}.toml"
        )

        for path in paths:
            assert path in conf.config_file_path
        assert paths == conf.config_file_path

    @staticmethod
    def test_init_macos():
        """Test init of a PrgConfig."""
        os.environ["XDG_CONFIG_HOME"] = "/home/test/.config"
        os.environ["XDG_CONFIG_DIRS"] = "/etc/xdg1:/etc/xdg2"
        prgname = "test-prg"
        conf = config.PrgConfig(prgname)
        # Spoof the platform function
        conf.getplatform = lambda: "Darwin"
        assert conf.getplatform() == "Darwin"
        conf.config_file_path = conf.default_config_file_path
        paths = []

        # Current directory
        paths.append(Path(f"{prgname}.conf"))
        paths.append(Path(f"{prgname}.toml"))
        paths.append(Path(f".{prgname}.conf"))
        paths.append(Path(f".{prgname}.toml"))

        # In MacOS dir
        paths.append(
            Path().home() / f"Library/Application Support/{prgname}.conf"
        )
        paths.append(
            Path().home() / f"Library/Application Support/{prgname}.toml"
        )
        paths.append(
            Path().home()
            / f"Library/Application Support/{prgname}/{prgname}.conf"
        )
        paths.append(
            Path().home()
            / f"Library/Application Support/{prgname}/{prgname}.toml"
        )

        # In xdg_config_home
        paths.append(Path(f"/home/test/.config/{prgname}.conf"))
        paths.append(Path(f"/home/test/.config/{prgname}.toml"))
        paths.append(Path(f"/home/test/.config/{prgname}/config"))
        paths.append(Path(f"/home/test/.config/{prgname}/{prgname}.conf"))
        paths.append(Path(f"/home/test/.config/{prgname}/{prgname}.toml"))

        # In home
        paths.append(Path().home() / f".{prgname}.conf")
        paths.append(Path().home() / f".{prgname}.toml")

        # In xdg_config_dirs
        paths.append(Path(f"/etc/xdg1/{prgname}.conf"))
        paths.append(Path(f"/etc/xdg1/{prgname}.toml"))
        paths.append(Path(f"/etc/xdg1/{prgname}/config"))
        paths.append(Path(f"/etc/xdg1/{prgname}/{prgname}.conf"))
        paths.append(Path(f"/etc/xdg1/{prgname}/{prgname}.toml"))
        paths.append(Path(f"/etc/xdg2/{prgname}.conf"))
        paths.append(Path(f"/etc/xdg2/{prgname}.toml"))
        paths.append(Path(f"/etc/xdg2/{prgname}/config"))
        paths.append(Path(f"/etc/xdg2/{prgname}/{prgname}.conf"))
        paths.append(Path(f"/etc/xdg2/{prgname}/{prgname}.toml"))

        # /etc
        paths.append(Path(f"/etc/{prgname}.conf"))
        paths.append(Path(f"/etc/{prgname}.toml"))
        paths.append(Path(f"/etc/{prgname}/config"))
        paths.append(Path(f"/etc/{prgname}/{prgname}.conf"))
        paths.append(Path(f"/etc/{prgname}/{prgname}.toml"))

        for path in paths:
            assert path in conf.config_file_path
        assert paths == conf.config_file_path

    @staticmethod
    def test_load():
        """Test load function"""
        with tempfile.TemporaryDirectory("prgconfig-test") as tmpdir:
            # Create default config file
            default = Path(tmpdir) / "default"
            default.write_text(
                """
                default = "value"
                item = "val"
                foo = "bar"

                [section]
                    key1 = "val1"
                """
            )
            confone = Path(tmpdir) / "confone"
            confone.write_text(
                """
                item = "value"
                foo = 1
                """
            )
            conftwo = Path(tmpdir) / "conftwo"
            conftwo.write_text(
                """
                foo = 2
                """
            )
            conferror = Path(tmpdir) / "conferror"
            conferror.write_text(
                """
                foo = 2:3
                """
            )
            confwrong = Path(tmpdir) / "confwrong"
            config_file_path = [confone, confwrong, str(conftwo)]
            conf = config.PrgConfig(
                prg_name="test",
                defaults_file=default,
                config_file_path=config_file_path,
            )
            conf.load()
            assert conf.config_sources == [confone, conftwo]
            assert conf.get("default") == "value"
            assert conf.get("item") == "value"
            assert conf.get("foo") == 1
            assert conf.get("section").get("key1") == "val1"
            # Test clear
            conf.clear()
            assert not conf
            assert not conf.config_sources
            # Test with merge set to false
            conf.merge = False
            conf.load()
            assert conf.config_sources == [confone]
            assert conf.get("default") == "value"
            assert conf.get("foo") == 1

    @staticmethod
    def test_load_with_errors():
        """Test that an error in config file correctly raise error"""
        with tempfile.TemporaryDirectory("prgconfig-test") as tmpdir:
            conferror = Path(tmpdir) / "conferror"
            conferror.write_text(
                """
                foo = 2:3
                """
            )
            conf = config.PrgConfig(
                prg_name="test", config_file_path=conferror
            )
            with pytest.raises(config.ParseError):
                conf.load()
            assert not conf.config_sources

    @staticmethod
    def test_defaults_with_errors():
        """Test an error in defaults file correctly raise error"""
        with tempfile.TemporaryDirectory("prgconfig-test") as tmpdir:
            conferror = Path(tmpdir) / "defaults"
            conferror.write_text(
                """
                foo = 2:3
                """
            )
            conf = config.PrgConfig(prg_name="test", defaults_file=conferror)
            with pytest.raises(config.ParseError):
                conf.load()

    @staticmethod
    def test_dumps():
        """Test dumping the config"""
        with tempfile.TemporaryDirectory("prgconfig-test") as tmpdir:
            original_conf = Path(tmpdir) / "defaults"
            original_conf.write_text(
                """default = "value"
item = "val"
foo = "bar"

[section]
    key1 = "val1"
                """
            )
            conf = config.PrgConfig(
                prg_name="test", defaults_file=original_conf
            )
            conf.load()
            output_conf = Path(tmpdir) / "output"
            output_conf.write_text(conf.dumps())
            assert filecmp.cmp(original_conf, output_conf)

    @staticmethod
    def test_getcheck():
        """Test getcheck method"""
        with tempfile.TemporaryDirectory("prgconfig-test") as tmpdir:
            original_conf = Path(tmpdir) / "defaults"
            original_conf.write_text(
                """
                default = "value"
                item = 2.0
                foo = "bar"

                [section]
                    key1 = 1.0
                """
            )
            conf = config.PrgConfig(
                prg_name="test", defaults_file=original_conf
            )
            conf.load()
            assert conf["default"] == "value"
            assert conf.getcheck("default", vtype=str) == "value"
            assert conf["item"] == 2.0
            assert conf.getcheck("item", vtype=float) == 2.0
            # Check subsection
            assert conf["section"]["key1"] == 1.0
            assert conf.getcheck("section", "key1", vtype=float) == 1.0
            # Check when keys does not exists
            assert conf.getcheck("aaa", "bbb") is None
            # Check functions raises error properly
            with pytest.raises(TypeError):
                conf.getcheck(vtype=dict)
            with pytest.raises(TypeError):
                conf.getcheck("section", "key1", vtype=str)
